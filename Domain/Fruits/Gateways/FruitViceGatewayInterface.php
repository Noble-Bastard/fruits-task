<?php

declare(strict_types=1);

namespace Domain\Fruits\Gateways;

interface FruitViceGatewayInterface
{
   public function getAllFruits();
}
