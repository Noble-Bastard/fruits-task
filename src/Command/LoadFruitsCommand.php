<?php

namespace App\Command;

use Domain\Fruits\Contracts\FruitServiceInterface;
use Domain\Fruits\Data\CreateFruitDto;
use Domain\Fruits\Gateways\FruitViceGatewayInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Summary of LoadFruitsCommand
 */
class LoadFruitsCommand extends Command
{
   protected static $defaultName = 'app:get-fruits';

   public function __construct(
      public readonly FruitServiceInterface $fruitService,
      public readonly FruitViceGatewayInterface $fruitViceGateway,
      public readonly MailerInterface $mailer
   ) {
      parent::__construct();
   }

   protected function configure()
   {
      $this->setDescription('Gets all fruits from fruityvice.com and saves them into the database.');
   }

   protected function execute(InputInterface $input, OutputInterface $output): int
   {
      $fruits = $this->fruitViceGateway->getAllFruits();
      foreach ($fruits as $fruit) {
         $this->fruitService->create(
            new CreateFruitDto(
               $fruit['name'],
               $fruit['family'],
               $fruit['order'],
               $fruit['genus'],
               $fruit['nutritions']['carbohydrates'],
               $fruit['nutritions']['protein'],
               $fruit['nutritions']['fat'],
               $fruit['nutritions']['sugar']
            )
         );
      }
      $email = (new Email())
         ->from('no-reply@manax.cloud')
         ->to('assabyn@gmail.com')
         ->cc('cc@example.com')
         ->bcc('bcc@example.com')
         //->replyTo('fabien@example.com')
         //->priority(Email::PRIORITY_HIGH)
         ->subject('New fruits Added!')
         ->text('New fruits');
         
      $this->mailer->send($email);

      return Command::SUCCESS;
   }
}
