<?php

declare(strict_types=1);

namespace Domain\Fruits\Data;

class GetFruitDto
{
    public function __construct(
        public readonly ?string $name = null,
        public readonly ?string $family = null,
    )
    {
    }
}
