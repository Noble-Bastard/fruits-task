<?php

declare(strict_types=1);

namespace Domain\Fruits\Contracts;

use Domain\Fruits\Data\CreateFruitDto;
use Domain\Fruits\Data\GetFruitDto;


interface FruitServiceInterface
{
   public function create(CreateFruitDto $dto): void;
   public function getFruits(GetFruitDto $dto);
   public function getFamilies();
}
