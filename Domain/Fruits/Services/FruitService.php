<?php

namespace Domain\Fruits\Services;

use Domain\Fruits\Contracts\FruitServiceInterface;
use Domain\Fruits\Data\CreateFruitDto;
use Domain\Fruits\Data\GetFruitDto;
use Domain\Fruits\Repositories\FruitRepositoryInterface;

class FruitService implements FruitServiceInterface
{

   public function __construct(
      public readonly FruitRepositoryInterface $fruitRepository,
   ) {
   }
   /**
    * @param \Domain\Fruits\Data\CreateFruitDto $dto
    * @return \Domain\Fruits\Entities\Fruit
    */
   public function create(CreateFruitDto $dto): void
   {
      $this->fruitRepository->save($dto);
   }

   /**
    *
    * @return mixed
    */
   public function getFruits(GetFruitDto $dto)
   {
      return $this->fruitRepository->get($dto);
   }

   public function getFamilies()
   {
      return $this->fruitRepository->getFamilies()->getResult();
   }
   
}
