<?php

declare(strict_types=1);

namespace Domain\User\Repositories;

use Domain\User\Entities\User;
/**
 * Summary of UserRepositoryInterface
 */
interface UserRepositoryInterface
{
    public function findOneBySomeField($value): ?User;
}
