<?php

namespace Infrastructure\Persistence\Repositories\Fruit;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Domain\Fruits\Data\GetFruitDto;
use Domain\Fruits\Entities\Fruit;
use Domain\Fruits\Repositories\FruitRepositoryInterface;
use Domain\Fruits\Data\CreateFruitDto;

/**
 * @extends ServiceEntityRepository<Fruit>
 *
 * @method Fruit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fruit[]    findAll()
 */
class FruitRepository extends ServiceEntityRepository implements FruitRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fruit::class);
    }

    public function save(CreateFruitDto $entity, bool $flush = true): void
    {
        // Надо переписать потом
        $fruit = new Fruit();
        $fruit->fruit_name = $entity->name;
        $fruit->family = $entity->family;
        $fruit->fruit_order = $entity->order;
        $fruit->genus = $entity->genus;
        $fruit->carbohydrates = $entity->carbohydrates;
        $fruit->protein = $entity->protein;
        $fruit->fat = $entity->fat;
        $fruit->sugar = $entity->sugar;
        $this->getEntityManager()->persist($fruit);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Fruit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getFamilies(): Query
    {
        $query = $this->createQueryBuilder("f")
            ->select("f.family")
            ->groupBy("f.family");

        return $query->getQuery();
    }

    public function get(GetFruitDto $getFruitDto): Query
    {
        $query = $this->createQueryBuilder('f');

        if (!is_null($getFruitDto->name))
            $query->andWhere('f.fruit_name = :val')
                ->setParameter('val', $getFruitDto->name);

        if (!is_null($getFruitDto->family))
            $query->andWhere('f.family= :parent')
                ->setParameter('parent', $getFruitDto->family);

        return $query->getQuery();
    }
}
