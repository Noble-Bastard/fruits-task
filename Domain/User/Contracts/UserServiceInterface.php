<?php

declare(strict_types=1);

namespace Domain\User\Contracts;

interface UserServiceInterface
{
   public function addFavouriteFruit(string $fruit, int $userId): bool;
   public function getFavouriteFruits(int $userId);
   public function getNutritionFacts(int $userId);

}
