<?php

declare(strict_types=1);

namespace Domain\Fruits\Repositories;

use Doctrine\ORM\Query;
use Domain\Fruits\Data\GetFruitDto;
use Domain\Fruits\Entities\Fruit;
use Domain\Fruits\Data\CreateFruitDto;

/**
 * Summary of FruitRepositoryInterface
 */
interface FruitRepositoryInterface
{
    public function save(CreateFruitDto $fruit, bool $flush = false): void;

    public function get(GetFruitDto $getFruitDto): Query;
    public function getFamilies(): Query;
}
