<?php

namespace Domain\Fruits\Entities;

use Doctrine\ORM\Mapping as ORM;
use Infrastructure\Persistence\Repositories\Fruit\FruitRepository;

#[ORM\Entity(repositoryClass: FruitRepository::class)]
#[ORM\Table(name: 'fruit')]
class Fruit
{
   #[ORM\Id]
   #[ORM\GeneratedValue]
   #[ORM\Column]
   public $id;

   #[ORM\Column(type: "string", length: 255)]
   public $fruit_name;

   #[ORM\Column(type: "string", length: 255)]
   public $family;

   #[ORM\Column(type: "string", length: 255)]
   public $fruit_order;

   #[ORM\Column(type: "string", length: 255)]
   public $genus;

   #[ORM\Column(type: "float")]
   public $carbohydrates;

   #[ORM\Column(type: "float")]
   public $protein;

   #[ORM\Column(type: "float")]
   public $fat;

   #[ORM\Column(type: "float")]
   public $sugar;
}
