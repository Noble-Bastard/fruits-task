<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Fruits\Contracts\FruitServiceInterface;
use Domain\Fruits\Repositories\FruitRepositoryInterface;
use Domain\User\Contracts\UserServiceInterface;
use Infrastructure\Persistence\Repositories\User\UserRepository;
use App\Utils\Paginator;
use Domain\Fruits\Data\GetFruitDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FruitsController extends AbstractController
{
    public function __construct(
        public readonly UserServiceInterface $userService,
        public readonly FruitServiceInterface $fruitService,
    ) {
    }

    #[Route('/{name}', name: 'filtered_by_name_fruits')]
    #[Route('/family/{family}', name: 'filtered_by_family_fruits')]
    #[Route('/', name: 'all_fruits')]
    public function allFruits(Request $request = null, Paginator $paginator): Response
    {
        return $this->render('fruits/index.html.twig', [
            'controller_name' => 'FruitsController',
            'paginator' => $paginator->paginate($this->fruitService->getFruits(new GetFruitDto($request->get('name'), $request->get('family'))), $request->query->getInt('page', 1)),
            'all_fruits' => $this->fruitService->getFruits(new GetFruitDto())->getResult(),
            'families' => $this->fruitService->getFamilies()
        ]);
    }


    #[Route('/fruits/favourite', name: 'favourite_fruits')]
    public function favouriteFruits(): Response
    {
        return $this->render('fruits/favourite_fruits.html.twig', [
            'controller_name' => 'FruitsController',
            'favourite_fruits' => $this->userService->getFavouriteFruits(1), // 1 is user id
            'nutrition_facts_sum' => $this->userService->getNutritionFacts(1)
        ]);
    }

    #[Route("/add_favourite/{name}", name: "add_favourite_fruit")]
    public function addFavouriteFruit(Request $request)
    {
        return $this->userService->addFavouriteFruit($request->get('name'), 1) === true ? $this->redirect('/fruits/favourite') : $this->redirect('/'); // 1 is user id
    }
}
