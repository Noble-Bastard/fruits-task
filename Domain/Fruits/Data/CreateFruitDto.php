<?php

declare(strict_types=1);

namespace Domain\Fruits\Data;

class CreateFruitDto
{
   public function __construct(
      public readonly string $name,
      public readonly string $family,
      public readonly string $order,
      public readonly string $genus,
      public readonly float $carbohydrates,
      public readonly float $protein,
      public readonly float $fat,
      public readonly float $sugar,
   ) {
   }
}
