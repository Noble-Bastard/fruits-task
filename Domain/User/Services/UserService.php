<?php

namespace Domain\User\Services;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Fruits\Data\GetFruitDto;
use Domain\Fruits\Repositories\FruitRepositoryInterface;
use Domain\User\Contracts\UserServiceInterface;
use Domain\User\Repositories\UserRepositoryInterface;

class UserService implements UserServiceInterface
{
   private const MAX_FAVOURITE_FRUITS_COUNT = 10;
   public function __construct(
      public readonly UserRepositoryInterface $userRepository,
      public readonly EntityManagerInterface $entityManager,
      public readonly FruitRepositoryInterface $fruitsRepository
   ) {
   }

   /**
    * @param \Domain\Fruits\Data\CreateFruitDto $dto
    * @return \Domain\Fruits\Entities\Fruit
    */
   public function addFavouriteFruit(string $fruit, int $userId): bool
   {
      $entity = $this->userRepository->findOneBySomeField(1);
      $favouriteFruits = $entity->getFavouriteFruits();
      if (count($favouriteFruits) >= self::MAX_FAVOURITE_FRUITS_COUNT or in_array($fruit, $favouriteFruits)) {
         return false;
      }
      $entity->setFavouriteFruits(array_merge($favouriteFruits, [$fruit]));
      $this->entityManager->persist($entity);
      $this->entityManager->flush();
      return true;
   }

   /**
    *
    * @param int $userId
    * @return mixed
    */
   public function getFavouriteFruits(int $userId)
   {
      return $this->userRepository->findOneBySomeField($userId)->getFavouriteFruits();
   }

   /**
    * @param int $userId
    * @return mixed
    */
   public function getNutritionFacts(int $userId)
   {
      $fruits = $this->getFavouriteFruits($userId);
      $nutritionFacts = 0;
      foreach ($fruits as $fruit) {
         $fruitEntity = $this->fruitsRepository->get(new GetFruitDto($fruit))->getOneOrNullResult();
         $nutritionFacts += $fruitEntity->carbohydrates + $fruitEntity->protein + $fruitEntity->sugar + $fruitEntity->fat;
      }
      return $nutritionFacts;
   }
}
