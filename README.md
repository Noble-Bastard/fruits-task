[Fruit Test Task](https://docs.google.com/document/d/1hLDAU9m6bFGLMq6ehbWGfNEEcnAf-4jfgjEOnOP-xH0/edit)


How to run 

```
composer install
```
Update db(mysql) creds in .env
```
php bin/console doctrine:database:create
```

```
php bin/console doctrine:migrations:migrate
```


Load Fruits from fruitvice.com
```
php bin/console app:get-fruits
```

Email async(run after loading fruits). In new window|process
```
php bin/console messenger:consume async
```


Run project
```
symfony server:start on Localhost
```
