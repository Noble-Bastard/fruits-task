<?php

namespace Infrastructure\Gateways\FruitVice;

use Domain\Fruits\Gateways\FruitViceGatewayInterface;
use Infrastructure\Gateways\FruitVice\Exceptions\FruitViceException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\ResponseInterface;

class FruitViceGateway implements FruitViceGatewayInterface
{
   private const GET_ALL_FRUIT_PATH = '/api/fruit/all';

   protected $baseUrl;
   protected $client;

   public function __construct()
   {
      $this->baseUrl = 'https://fruityvice.com';
      $this->client = HttpClient::create();
   }

   /**
    * @throws FruitViceException
    */
   public function getAllFruits()
   {
      $response = $this->get(self::GET_ALL_FRUIT_PATH);

      if ($response->getStatusCode() !== 200) {
         $this->throw($response);
      }

      $data = json_decode($response->getContent(), true);

      return $data;
   }

   /**
    * @throws FruitViceException
    */
   private function throw(ResponseInterface $response)
   {
      throw new FruitViceException($response->getStatusCode(), $response->getContent());
   }

   private function get(string $path): ResponseInterface
   {
      $url = $this->baseUrl . $path;

      return $this->client->request('GET', $url);
   }
}
