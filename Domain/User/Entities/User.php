<?php

namespace Domain\User\Entities;

use Infrastructure\Persistence\Repositories\User\UserRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'user')]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public $id;

    #[ORM\Column(nullable: true)]
    private ?array $favourite_fruits = [];

    public function getFavouriteFruits(): ?array
    {
        return $this->favourite_fruits ?? [];
    }

    public function setFavouriteFruits(?array $favourite_fruits): self
    {
        $this->favourite_fruits = $favourite_fruits;

        return $this;
    }
}
